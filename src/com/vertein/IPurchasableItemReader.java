package com.vertein;

import java.util.List;

public interface IPurchasableItemReader{
    
    /**
     * 
     * @return the list of items from input source provided in constructor
     */
    public List<PurchasableItem> getPurchasableItems();
    
}
