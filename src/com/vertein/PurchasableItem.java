package com.vertein;

import java.math.BigDecimal;


/**
 * 
 * An object representing an item to be purchased
 *
 */
public class PurchasableItem{
    
    private String name;
    private BigDecimal price;
    private boolean isExempt;
    private boolean isImported;
    private int quantity;
    
    /**
     * Constructs an Item object
     * @param name
     * @param price - Results not guaranteed for price less than zero.
     * @param isExempt
     * @param isImported
     * @param quantity - Results not guaranteed for quantity less than zero.
     */
    public PurchasableItem(String name, BigDecimal price, boolean isExempt, boolean isImported, int quantity){
        this.name = name;
        //Price cannot be negative
        this.price = price.max(BigDecimal.ZERO);
        this.isExempt = isExempt;
        this.isImported = isImported;
        //quantity cannot be negative
        this.quantity = quantity<0 ? 0:quantity;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @return if item is exempt
     */
    public boolean isExempt() {
        return isExempt;
    }

    /**
     * @return if item is imported
     */
    public boolean isImported() {
        return isImported;
    }


    /**
     * @return the quantity of items
     */
    public int getQuantity() {
        return quantity;
    }
    
}