package com.vertein;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * 
 * An implementation to read items from a file based source
 *
 */
public class PurchasableItemFileReaderImpl implements IPurchasableItemReader{

    private final List<PurchasableItem> items;
    private static HashSet<String> exemptDictionary;
    private static final String IMPORTED_KEYWORD="imported";
    
    //Statically load the names of items that are exempt
    static{
        File exemptNamesFile = new File("exemptNames.txt");
        FileInputStream fis;
        HashSet<String> workingExemptDictionary = new HashSet<String>();
        try{
            fis = new FileInputStream(exemptNamesFile);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String exemptName = null;
            while((exemptName = br.readLine()) != null){
                workingExemptDictionary.add(exemptName.toLowerCase());
            }
            br.close();
        }catch(IOException e){
            e.printStackTrace();
        }
        exemptDictionary = workingExemptDictionary;
    }
    
    /**
     * 
     * Assumes that the input file has 1 item per line.
     * Assumes the line is in <Quantity> <Name of Item> <at> <price> format
     * @param inputFile - File to read that has purchased items
     * 
     */
    public PurchasableItemFileReaderImpl(File inputFile){
        FileInputStream fis;
        List<PurchasableItem> fileItems = new ArrayList<PurchasableItem>();
        try {
            fis = new FileInputStream(inputFile);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line = null;
            while((line = br.readLine()) !=null){
                //Every line represents an purchasable Item.
                String delims = "[ ]+";
                String[] tokens = line.split(delims);
                //We assume that the line has at least 4 'words'
                //quantity, name, the word 'at', and price
                //if not, skip line
                if(tokens.length < 4){
                    continue;
                }
                int quantity = Integer.parseInt(tokens[0]);
                BigDecimal price = new BigDecimal(tokens[tokens.length-1]);
                String name = "";
                boolean isImported = false;
                boolean isExempt = false;
                for(int i=1; i<tokens.length-2; i++){
                    if(tokens[i].equalsIgnoreCase(IMPORTED_KEYWORD)){
                        isImported = true;
                    }else if(exemptDictionary.contains(tokens[i])){
                        isExempt = true;
                    }
                    name = name.concat(tokens[i] + " ");
                }
                PurchasableItem myItem = new PurchasableItem(name.trim(), price, 
                        isExempt, isImported, quantity);
                fileItems.add(myItem);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.items = fileItems;
    }

    @Override
    public List<PurchasableItem> getPurchasableItems() {
        return this.items;
    }
    
}
