package com.vertein;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class Receipt{
    
    private List<PurchasableItem> items;
    private final Double basicTax;
    private final Double importTax;
    private static final BigDecimal NEARESTROUND = BigDecimal.valueOf(.05);
    
    /**
     * 
     * @param itemsBought
     * @param basicSalesTax - basic sales tax represented as double.  IE 5% 
     * should be .05  Any value less than 0.00 will be treated as 0.00
     * @param importSalesTax - import sales tax represented as double. IE 10%
     * should be .1   Any value less than 0.00 will be treated as 0.00
     */
    public Receipt(List<PurchasableItem> itemsBought, Double basicSalesTax,
            Double importSalesTax){
        this.items = itemsBought;
        this.basicTax = basicSalesTax<0 ? 0:basicSalesTax;
        this.importTax = importSalesTax<0 ? 0:importSalesTax;
    }
    
    /**
     * Will write in this format <quantity> <name of item> <:> <price including tax>
     * ex: 1 hotel room : 109.99
     * @param outputFile where to write the receipt to
     * 
     */
    public void printReceiptToFile(File outputFile){
        BigDecimal taxes = BigDecimal.ZERO;
        BigDecimal totalAmount = BigDecimal.ZERO;
        BufferedWriter output = null;
        try{
            output = new BufferedWriter(new FileWriter(outputFile));
        
            for(PurchasableItem item: items){
                BigDecimal itemPrice = item.getPrice();
                
                //If item is exempt, no tax levied.  Else use the tax specified from constructor
                //use the roundTaxes function to round the tax up to next .05
                BigDecimal itemBasicTax = roundTaxes(item.isExempt()? BigDecimal.ZERO:itemPrice.multiply(BigDecimal.valueOf(this.basicTax)));
                
                //If item was not imported, no tax levied.  Else use the tax specified from constructor
                //use the roundTaxes function to round the tax up to next .05
                BigDecimal itemImportTax = roundTaxes(!item.isImported() ? BigDecimal.ZERO:itemPrice.multiply(BigDecimal.valueOf(this.importTax)));
                //Item's total tax is a sum of import and sales tax
                BigDecimal itemTotalTax = itemBasicTax.add(itemImportTax);
                //Item's final price is a sum of tax and original price
                BigDecimal itemFinalPrice = itemPrice.add(itemTotalTax);
                
                //Update total amount of receipt
                totalAmount = totalAmount.add(itemFinalPrice);
                //Update total amount of taxes
                taxes = taxes.add(itemTotalTax);
                
                //Write item line to output file
                output.write(item.getQuantity()+ " " + item.getName()+" : " + itemFinalPrice + "\n");
            }
            //Write Taxes line to output file
            output.write("Sales Taxes: "+taxes+"\n");
            //Write total line to output file
            output.write("Total: "+totalAmount+"\n");
        }catch(IOException e){
            e.printStackTrace();
        }finally{
            //Close the output stream
            try {
                if(output !=null){
                    output.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Rounds a value up to the next .05
     * 
     * @param amount the amount to be rounded
     * @return amount rounded up to the .05
     */
    private BigDecimal roundTaxes(BigDecimal amount){
        BigDecimal divided = amount.divide(NEARESTROUND, 0, RoundingMode.UP);
        BigDecimal result = divided.multiply(NEARESTROUND);
        return result;
    }
    
}