
package com.vertein;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;


public class ReceiptGenerator{
    
    //Property Names
    private static final String BASIC_SALES_TAX_PROP_NAME = "basicSalesTax";
    private static final String IMPORT_SALES_TAX_PROP_NAME = "importSalesTax";
    
    public static void main(String[] args){
        
        //Will use this implementation of the PurchasableItemReader
        PurchasableItemFileReaderImpl myItemFileReader;
        
        //Load the tax values and use default values of 0 for tax
        Double basicSalesTax = 0.0;
        Double importSalesTax = 0.0;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            if(input!=null){
                try{
                    input.close();
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
        }
        
        //If prop was initialized and now contains the property values
        if(prop!=null){
            String basicTax = prop.getProperty(BASIC_SALES_TAX_PROP_NAME);
            //basicTax will be null if property does not exist in config.properties
            if(basicTax!=null){
                basicSalesTax = Double.valueOf(basicTax);
            }
            String importTax = prop.getProperty(IMPORT_SALES_TAX_PROP_NAME);
            //importTax will be null if property does not exist in config.properties
            if(importTax!=null){
                importSalesTax = Double.valueOf(importTax);
            }
        }
        
        
        for(int i=0; i<args.length; i++){
            File file = new File(args[i]);
            myItemFileReader = new PurchasableItemFileReaderImpl(file);
            List<PurchasableItem> itemList = myItemFileReader.getPurchasableItems();
            Receipt myReceipt = new Receipt(itemList, basicSalesTax, importSalesTax);
            File outputFile = new File("output"+(i+1));
            myReceipt.printReceiptToFile(outputFile);
            
        }
        
    }
    
    
}
